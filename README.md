# Docs

Source documents (.docx) & exports (pdf, images, ...) of the project

# Programmes requis pour éditer les formats :

* `.docx` : Word
* `.bmpr` : Balsamiq
* `.drawio` : Draw.io
* `.xcf` : Gimp
* `.pptx` et `.ppsx` : PowerPoint


# Répertoire sur les serveurs de l'école 
Les fichiers sources sont également disponible sur les serveurs de l'école, dans le dossier : `file:\\intra.he-arc.ch\ORG\ING\Formation\200_Bachelor\240_Niveau-2\241_Etudiants\2281_Projet_P2_SA\Groupe_5 RAE\Fichiers sources`
